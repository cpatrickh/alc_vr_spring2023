using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BlockColor
{
    Green,
    Red
}

public class Blockscript : MonoBehaviour
{
   public BlockColor color;
   
   public GameObject brokenBlockLeft;
   public GameObject brokenBlockRight;
   public float brokenBlockForce;
   public float brokenBlockTorque;
   public float brokenBlockDestroyDelay;
   
   private void OnTriggerEnter(Collider other)
   {
        if(other.CompareTag("sword red"))
        {
            if(color == BlockColor.Red)
            {
                GameManager.instance.AddScore();
            }
            else
            {
                GameManager.instance.HitWrongBlock();
            }
            Hit();
        }
        else if(other.CompareTag("sword green"))
        {
            if(color == BlockColor.Green)
            {
                GameManager.instance.AddScore();
            }
            else
            {
                GameManager.instance.HitWrongBlock();
            }
            Hit();
        }
   }
   
   public void Hit()
   {
        //Enable the broken pieces
        brokenBlockLeft.SetActive(true);
        brokenBlockRight.SetActive(true);
        
        //Remove them as children
        brokenBlockLeft.transform.parent = null;
        brokenBlockRight.transform.parent = null;
        
        //Add force to the blocks
        Rigidbody leftRig = brokenBlockLeft.GetComponent<Rigidbody>();
        Rigidbody rightRig = brokenBlockRight.GetComponent<Rigidbody>();
        
        leftRig.AddForce(-transform.right * brokenBlockForce, ForceMode.Impulse);
        rightRig.AddForce(transform.right * brokenBlockForce, ForceMode.Impulse);
        
        //Add torque to the pieces
        leftRig.AddTorque(-transform.forward * brokenBlockTorque, ForceMode.Impulse);
        rightRig.AddTorque(transform.forward * brokenBlockTorque, ForceMode.Impulse);
        
        //Destroy the broken pieces after a few seconds
        Destroy(brokenBlockLeft, brokenBlockDestroyDelay);
        Destroy(brokenBlockRight, brokenBlockDestroyDelay);
        
        //Destroy the main block
        Destroy(gameObject);
   }
}
