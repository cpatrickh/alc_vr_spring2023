using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissHitDetector : MonoBehaviour
{
    void OnTriggerEnter (Collider other)
    {
        if(other.CompareTag("Block"))
        {
            other.GetComponent<Blockscript>().Hit();
            GameManager.instance.MissBlock();
        }
    }
}