using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarSpawner : MonoBehaviour
{
    public GameObject car;
    public GameObject placementIndicator;
    public GameObject carControls;
    public GameObject placeCarButton;
    
    void start()
    {
        car.SetActive(false);
        carControls.SetActive(false);
        placementIndicator.SetActive(true);
        placeCarButton.SetActive(true);
    }

    public void OnPlaceCarButton()
    {
        car.SetActive(true);
        carControls.SetActive(true);
        placementIndicator.SetActive(false);
        placeCarButton.SetActive(false);

        car.transform.position = placementIndicator.transform.position;
    }
}
