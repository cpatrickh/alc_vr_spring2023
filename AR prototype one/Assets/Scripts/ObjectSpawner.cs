using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawner : MonoBehaviour
{
    public GameObject objectToSpawn;
    private PlacementIndicator PlacementIndicator;

    void start()
    {
        PlacementIndicator = FindObjectOfType<PlacementIndicator>();
    }

    void update()
    {
        if(Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began)
        {
            GameObject obj = Instantiate(objectToSpawn, PlacementIndicator.transform.position, PlacementIndicator.transform.rotation);
        }
    }
}
